import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  login(user) {
    return this.http.post(`${environment.api_base_url}/login`,user);
  }
  createDataset(payload) {
    return this.http.post(`${environment.api_base_url}/dataset`,payload);
  }
  tranformDataset(ds,payload) {
    return this.http.post(`${environment.api_base_url}/dataset/transform/${ds}`,payload);
  }
  createMySQLDataset(payload) {
    return this.http.post(`${environment.api_base_url}/mysql/create`,payload);
  }
  getMySQLDBs(payload) {
    return this.http.post(`${environment.api_base_url}/mysql/list`,payload);
  }
  generateDataset(payload) {
    return this.http.post(`${environment.api_base_url}/dataset/generate`,payload);
  }
  getGeneratedDatasets(){
    return this.http.get(`${environment.api_base_url}/dataset/generate/list/${localStorage.getItem('un')}`);
  }
  getUserDatasetList(type = null) {
    if(type){
      return this.http.get(`${environment.api_base_url}/dataset/list/${localStorage.getItem('un')}?provider=${type}`);
    }else{
      return this.http.get(`${environment.api_base_url}/dataset/list/${localStorage.getItem('un')}`);
    }
  }
  getDataset(ds) {
    return this.http.get(`${environment.api_base_url}/dataset/${ds}`,);
  }
  getMysqlDatasetTable(ds) {
    return this.http.get(`${environment.api_base_url}/dataset/mysql/${ds.dataset}/${ds.table}`,);
  }
  getMysqlDatasetTableChildren(payload) {
    return this.http.post(`${environment.api_base_url}/dataset/mysql/table/children`,payload);
  }
  getGeneratedDataset(ds){
    return this.http.get(`${environment.api_base_url}/dataset/generate/${ds}`,);
  }
}

