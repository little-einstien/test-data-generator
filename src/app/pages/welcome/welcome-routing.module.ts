import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateDatasetComponent } from './create-dataset/create-dataset.component';
import { DatasetViewComponent } from './dataset-view/dataset-view.component';
import { GenerateNewDatasetComponent } from './generate-new-dataset/generate-new-dataset.component';
import { SavedDatasetComponent } from './saved-dataset/saved-dataset.component';
import { ViewDatasetComponent } from './view-dataset/view-dataset.component';
import { ViewMysqlTableComponent } from './view-mysql-table/view-mysql-table.component';
import { WelcomeComponent } from './welcome.component';

const routes: Routes = [
  {
    path: '', component: WelcomeComponent,
    children: [
      { path: 'create-dataset', component: CreateDatasetComponent },
      { path: 'saved-dataset', component: SavedDatasetComponent },
      { path: 'view-dataset', component: ViewDatasetComponent },
      { path: 'dataset', component: DatasetViewComponent},
      { path: 'mysql-dataset', component: ViewMysqlTableComponent},
      { path: 'generate-dataset', component: GenerateNewDatasetComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
