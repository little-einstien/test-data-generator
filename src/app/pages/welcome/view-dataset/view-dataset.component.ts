import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-view-dataset",
  templateUrl: "./view-dataset.component.html",
  styleUrls: ["./view-dataset.component.css"],
})
export class ViewDatasetComponent implements OnInit {
  mapOfExpandData: { [key: string]: boolean } = {};
  listOfData = [];
  mysqlTableList = [];
  expandSet = new Set<number>();
  constructor(private apiService: ApiService, private router: Router) {}

  ngOnInit() {
    this.apiService.getUserDatasetList().subscribe((list: any) => {
      this.listOfData = list;
    });
  }
  view(dataset) {
    localStorage.setItem("dataset", dataset);
    this.router.navigate(["/dataset"]);
  }
  onExpandChange(id, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }
  getMySQLTables(name) {
    this.apiService.getDataset(name).subscribe((result: any) => {
      console.log(result);
      this.mysqlTableList = result.data;
    });
  }
  viewMySqlTable(dataset, table) {
    localStorage.setItem(
      "mysql-dataset",
      JSON.stringify({
        dataset: dataset,
        table: table,
      })
    );
    this.router.navigate(["/mysql-dataset"]);
  }
}
