import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { CreateDatasetComponent } from './create-dataset/create-dataset.component';
import { SavedDatasetComponent } from './saved-dataset/saved-dataset.component';
import { ViewDatasetComponent } from './view-dataset/view-dataset.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { WelcomeRoutingModule } from './welcome-routing.module';

import { WelcomeComponent } from './welcome.component';
import en from '@angular/common/locales/en';
import { registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatasetViewComponent } from './dataset-view/dataset-view.component';
import { ViewMysqlTableComponent } from './view-mysql-table/view-mysql-table.component';
import { GenerateNewDatasetComponent } from './generate-new-dataset/generate-new-dataset.component';
registerLocaleData(en);

@NgModule({
  imports: [WelcomeRoutingModule, NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule, CommonModule],
  declarations: [WelcomeComponent, CreateDatasetComponent, SavedDatasetComponent, ViewDatasetComponent, DatasetViewComponent, ViewMysqlTableComponent, GenerateNewDatasetComponent],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  exports: [WelcomeComponent]
})
export class WelcomeModule { }
