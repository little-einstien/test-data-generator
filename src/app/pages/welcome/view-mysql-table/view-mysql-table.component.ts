import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-view-mysql-table",
  templateUrl: "./view-mysql-table.component.html",
  styleUrls: ["./view-mysql-table.component.css"],
})
export class ViewMysqlTableComponent implements OnInit {
  public data = [];
  public headers = [];
  public pageSize = 10;
  public pagination = true;
  public total = 0;
  public loading = false;
  constructor(private apiService: ApiService, private router: Router) {}
  ngOnInit() {
    this.loading = true;
    this.apiService
      .getMysqlDatasetTable(JSON.parse(localStorage.getItem("mysql-dataset")))
      .subscribe((result: any) => {
        this.total = result.data.length;
        this.data = result.data;
        this.loading = false;
        this.headers = Object.keys(this.data[0]);
      });
  }
}
