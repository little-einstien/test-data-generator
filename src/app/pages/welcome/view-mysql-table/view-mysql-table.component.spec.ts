import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMysqlTableComponent } from './view-mysql-table.component';

describe('ViewMysqlTableComponent', () => {
  let component: ViewMysqlTableComponent;
  let fixture: ComponentFixture<ViewMysqlTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMysqlTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMysqlTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
