import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedDatasetComponent } from './saved-dataset.component';

describe('SavedDatasetComponent', () => {
  let component: SavedDatasetComponent;
  let fixture: ComponentFixture<SavedDatasetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedDatasetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedDatasetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
