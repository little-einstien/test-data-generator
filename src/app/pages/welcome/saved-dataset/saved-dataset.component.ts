import { Component, OnInit } from "@angular/core";
import { NzNotificationService } from "ng-zorro-antd";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-saved-dataset",
  templateUrl: "./saved-dataset.component.html",
  styleUrls: ["./saved-dataset.component.css"],
})
export class SavedDatasetComponent implements OnInit {
  public parentDS;
  public childDS;
  public fkey;
  public table;
  public rows;
  public datasets;
  public name;
  public initdata = {};
  public fields = [];
  public gdataset = "";
  public finalData = [];

  public models = [
    {
      parentDS: {},
      childDS: {},
      fkey: "",
      fields: [],
    },
  ];
  getKeys(obj) {
    return Object.keys(obj);
  }
  isVisible = false;

  constructor(
    private apiService: ApiService,
    private notification: NzNotificationService
  ) {}

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log("Button ok clicked!");
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log("Button cancel clicked!");
    this.isVisible = false;
  }

  ngOnInit() {
    this.apiService.getGeneratedDatasets().subscribe((list: any) => {
      console.log(list);
      this.initdata = list;
      this.gdataset = list.list[0].name;
      Object.keys(list.data).forEach((element) => {
        this.finalData.push({
          name: element,
          data: list.data[element],
          headers: Object.keys(list.data[element][0]),
        });
      });
    });
  }

  updateDataset(e) {
    this.apiService.getGeneratedDataset(e).subscribe((data) => {
      this.finalData = [];
      Object.keys(data).forEach((element) => {
        this.finalData.push({
          name: element,
          data: data[element],
          headers: Object.keys(data[element][0]),
        });
      });
      this.isVisible = false;
    });
  }

  export() {
    for (let i = 0; i < this.finalData.length; i++) {
      this.downloadFile(
        this.finalData[i].data,
        this.finalData[i].headers,
        this.finalData[i].name
      );
    }
  }
  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    let str = "";
    let row = "S.No,";
    for (let index in headerList) {
      row += headerList[index] + ",";
    }
    row = row.slice(0, -1);
    str += row + "\r\n";
    for (let i = 0; i < array.length; i++) {
      let line = i + 1 + "";
      for (let index in headerList) {
        let head = headerList[index];
        line += "," + array[i][head];
      }
      str += line + "\r\n";
    }
    return str;
  }
  downloadFile(data, headers, filename) {
    let csvData = this.ConvertToCSV(data, headers);
    console.log(csvData);
    let blob = new Blob(["\ufeff" + csvData], {
      type: "text/csv;charset=utf-8;",
    });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    // let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    // if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
    //     dwldLink.setAttribute("target", "_blank");
    // }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }
  transformation = {
    type: "",
  };
  transform() {
    let ds = this.gdataset;
    this.apiService
      .tranformDataset(ds, { transformation: this.transformation })
      .subscribe((data) => {
        console.log(data);
        this.notification.create(
          "success",
          "Success !",
          "Your Current Dataset is transformed and new column is added"
        );
        this.apiService.getGeneratedDatasets().subscribe((list: any) => {
          console.log(list);
          this.initdata = list;
          this.gdataset = list.list[0].name;
          this.finalData = []
          Object.keys(list.data).forEach((element) => {
            this.finalData.push({
              name: element,
              data: list.data[element],
              headers: Object.keys(list.data[element][0]),
            });
          });
        });
        this.isVisible = false;
      });
  }
  tableHeaders = [];
  ops = [
    {
      label: "Concatenate",
      value: "1",
    },
    {
      label: "Add",
      value: "2",
    },
  ];
  getHeaders(e) {
    this.finalData.forEach((ele) => {
      if (ele.name == e) {
        this.tableHeaders = ele.headers;
      }
    });
  }
}
