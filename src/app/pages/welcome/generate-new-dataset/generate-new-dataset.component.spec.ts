import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateNewDatasetComponent } from './generate-new-dataset.component';

describe('GenerateNewDatasetComponent', () => {
  let component: GenerateNewDatasetComponent;
  let fixture: ComponentFixture<GenerateNewDatasetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateNewDatasetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateNewDatasetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
