import { Component, OnInit } from "@angular/core";
import { NzNotificationService } from "ng-zorro-antd";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-generate-new-dataset",
  templateUrl: "./generate-new-dataset.component.html",
  styleUrls: ["./generate-new-dataset.component.css"],
})
export class GenerateNewDatasetComponent implements OnInit {
  public models = [
    {
      parentDS: {},
      childDS: {},
      fkey: "",
      fields: [],
    },
  ];
  public datasets;
  public mysqlTableList = [];
  public mysqlChildrenTable = [];
  public parentDS;
  public childDS;
  public fkey;
  public table;
  public rows;
  public name;
  public initdata = {};
  public fields = [];
  public gdataset = "";
  public finalData = [];

  constructor(
    private apiService: ApiService,
    private notification: NzNotificationService
  ) {}

  ngOnInit() {}
  getDatasets(e) {
    this.apiService.getUserDatasetList(e).subscribe((list: any) => {
      this.datasets = list;
    });
  }
  getDatasetDetails(e) {
    if (e.provider == "2") {
      this.apiService.getDataset(e.name).subscribe((result: any) => {
        console.log(result);
        this.mysqlTableList = result.data;
      });
    }
  }
  getTableChildren(ds, e) {
    this.apiService
      .getMysqlDatasetTableChildren({
        ds: ds,
        table: e,
      })
      .subscribe((result: any) => {
        console.log(result);
        this.mysqlChildrenTable = result.data;
      });
  }
  generate() {
    let data = {
      relationships: this.models,
      name: this.name,
      rows: this.rows,
      un: localStorage.getItem("un"),
    };
    this.notification.create(
      "success",
      "Started",
      "Synthetic Dataset Generation Started"
    );
    this.apiService.generateDataset(data).subscribe((resp) => {
      console.log(resp);
      this.apiService.getGeneratedDatasets().subscribe((list: any) => {
        console.log(list);
        this.initdata = list;
        this.gdataset = list.list[0].name;
        Object.keys(list.data).forEach((element) => {
          this.finalData.push({
            name: element,
            data: list.data[element],
            headers: Object.keys(list.data[element][0]),
          });
        });
        this.notification.create(
          "success",
          "Finished",
          "Synthetic Dataset Generation Finished"
        );
      });
    });
    console.log(data);
  }
  addMoreRelationship() {
    this.models.push({
      parentDS: "",
      childDS: "",
      fkey: "",
      fields: [],
    });
  }
  getDatasetCols(e, i) {
    console.log(e);
    this.apiService.getDataset(e.name).subscribe((result: any) => {
      this.models[i].fields = Object.keys(result.data[0]);
    });
  }
}
