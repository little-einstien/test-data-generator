import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { ApiService } from "src/app/api.service";

@Component({
  selector: "app-create-dataset",
  templateUrl: "./create-dataset.component.html",
  styleUrls: ["./create-dataset.component.css"],
})
export class CreateDatasetComponent implements OnInit {
  public db;
  public table;
  public dbList = [];
  constructor(
    private fb: FormBuilder,
    private APIService: ApiService,
    private notification: NzNotificationService,
    private router: Router
  ) {}
  form!: FormGroup;
  mySqlForm!: FormGroup;
  ngOnInit() {
    this.form = this.fb.group({
      provider: [null],
      name: [null],
      file: [null],
      pk: [null],
    });

    this.mySqlForm = this.fb.group({
      host: [null],
      uname: [null],
      pass: [null],
      db: [null],
    });
  }
  create() {
    if (this.form.get("provider").value == "1") {
      const formData = new FormData();
      formData.append("file", this.form.get("file").value);
      formData.append("provider", this.form.get("provider").value);
      formData.append("name", this.form.get("name").value);
      formData.append("un", localStorage.getItem("un"));
      formData.append("pk", this.form.get("pk").value);
      this.APIService.createDataset(formData).subscribe((resp) => {
        console.log(resp);
        this.notification.create("success", "Success", "Dataset Created");
        this.router.navigate(["/pages/view-dataset"]);
      });
    } else if (this.form.get("provider").value == "2") {
      let data = {
        provider: this.form.get("provider").value,
        name: this.form.get("name").value,
        host: this.mySqlForm.get("host").value,
        uname: this.mySqlForm.get("uname").value,
        pass: this.mySqlForm.get("pass").value,
        db: this.mySqlForm.get("db").value,
        un : localStorage.getItem("un")
      };
      this.APIService.createMySQLDataset(data).subscribe((resp) => {
        console.log(resp);
        this.notification.create("success", "Success", "Dataset Created");
        this.router.navigate(['/pages/view-dataset'])
      });
    }
  }
  getMySQLDBs() {
    this.APIService.getMySQLDBs({
      host: this.mySqlForm.get("host").value,
      uname: this.mySqlForm.get("uname").value,
      pass: this.mySqlForm.get("pass").value,
    }).subscribe((resp: [any]) => {
      console.log(resp);
      this.dbList = resp;
    });
  }
  get provider() {
    return this.form.controls.provider.value;
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      console.log("file selected");
      const file = event.target.files[0];
      this.form.get("file").setValue(file);
    }
  }
}
