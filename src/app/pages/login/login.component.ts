import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(private fb: FormBuilder, private router: Router, private APIService: ApiService) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
  login() {
    let user = {
      username: this.validateForm.controls.userName.value,
      password: this.validateForm.controls.password.value,
      remember: this.validateForm.controls.remember.value
    };
    console.log(user);
    this.APIService.login(user).subscribe((resp) => {
      if(resp && resp['1']){
        localStorage.setItem('un',user.username)
        this.router.navigate(['/pages/create-dataset']);
      }
    });
  }
}
